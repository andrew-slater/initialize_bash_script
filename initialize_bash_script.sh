#!/usr/bin/env bash

# Obtained from: https://gitlab.com/andrew-slater/initialize_bash_script
# Script to source at the start of a bash script to parse command line arguments etc
# Derived from: https://github.com/kvz/bash3boilerplate
# http://bash3boilerplate.sh/#authors
# The MIT License (MIT)
# Copyright (c) 2013 Kevin van Zonneveld and contributors

# Usage:
#  LOG_LEVEL=7 source initialize_bash_script.sh #where __usage and __helptext are defined in environment before sourcing this file (see below for examples)


# Exit on error. Append "|| true" to a particular command if you expect it to exit non-zero and want script to continue
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined variables. Use ${VAR:-} to use a potentially undefined variable - see definition of LOG_LEVEL below for example
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace
# Standardize locale so sort and join will be compatible
export LC_ALL=C

# Set magic variables for current file, directory, os, etc.
# Directory where this script is located
__dir="$(cd "$(dirname "${0}")" && pwd)"
# Full path to this script
__file="${__dir}/$(basename "${0}")"
# This script's filename without .sh extension
__base="$(basename "${__file}" .sh)"

# shellcheck disable=SC2034,SC2015
__invocation="$(printf %q "${__file}")$( (($#)) && printf ' %q' "$@" || true)"

# Define the environment variables (and their defaults) that this script depends on
LOG_LEVEL="${LOG_LEVEL:-6}" # 7 = debug -> 0 = emergency
NO_COLOR="${NO_COLOR:-}"    # true = disable color. otherwise autodetected

### Functions
##############################################################################

function __b3bp_log () {
  # Best practice to scope variables within functions locally so don't clash with global (and don't use the global __ prefix convention)
  # scoping can get confusing with regard to "shell/sub-shell" since not always obvious when a sub-shell is spawned (e.g. piping can cause it)
  # generally sub-shells inherit parent shell's environment (including variables) but can't manipulate parent shell's enviro
  # Functions can accept parameters the same way scripts do command line arguments e.g. $1 $2 where $0 is the name of the function
  local log_level="${1}"
  # Can "pop" parameters off $@ using shift n (n defaults to 1)
  shift
  # After shift n, first n parameters gone and now $1 contains (n+1)st parameter etc ($0 is excluded from this operation)

  # shellcheck disable=SC2034
  local color_debug="\\x1b[35m"
  # shellcheck disable=SC2034
  local color_info="\\x1b[32m"
  # shellcheck disable=SC2034
  local color_notice="\\x1b[34m"
  # shellcheck disable=SC2034
  local color_warning="\\x1b[33m"
  # shellcheck disable=SC2034
  local color_error="\\x1b[31m"
  # shellcheck disable=SC2034
  local color_critical="\\x1b[1;31m"
  # shellcheck disable=SC2034
  local color_alert="\\x1b[1;37;41m"
  # shellcheck disable=SC2034
  local color_emergency="\\x1b[1;4;5;37;41m"

  local colorvar="color_${log_level}"
  # Example of indirect referencing: http://tldp.org/LDP/abs/html/ivr.html
  # !colorvar is interpretted to the variable whose name is the value of the colorvar variable
  # So, if log_level is something like "trace" then colorvar would be defined as "color_trace" in prior command and this would be interpretted as:
  # local color="${color_trace:-${color_error}}" where there is no color_trace variable defined so the value of color_error is assigned to color instead
  # equivalent to ${${colorvar}} since nesting isn't supported
  local color="${!colorvar:-${color_error}}"
  local color_reset="\\x1b[0m"

  if [[ "${NO_COLOR:-}" == "true" ]] || { [[ "${TERM:-}" != "xterm"* ]] && [[ "${TERM:-}" != "screen"* ]]; } || [[ ! -t 2 ]]; then
    if [[ "${NO_COLOR:-}" != "false" ]]; then
      # Don't use colors on pipes or non-recognized terminals
      color=""; color_reset=""
    fi
  fi

  # all remaining arguments are to be printed
  local log_line=""
  # while loop example
  # similar until loop syntax where continues until condition true (instead of while condition true)
  # input to read command is the <<< "here string" (term if want to look up) after done where $@ is the command line args that haven't been popped off yet
  while IFS=$'\n' read -r log_line; do
    # here stdout (1) is re-directed to stderr (2)
    # &>file would re-direct both stdout & stderr to file
    # when using pipes, only sdtout is piped so if want everything including stderr piped, command1 2>&1 | command2
    echo -e "$(date -u +"%Y-%m-%d %H:%M:%S UTC") ${color}$(printf "[%9s]" "${log_level}")${color_reset} ${log_line}" 1>&2
    # if above echo string had not been quoted, would be problematic because of special characters that bash may interpret like : (these would need to be escaped e.g. \; for literal semi-colon instead of command termination)
    # single quotes treats entire string literally without any interpretation so no escaping required
    # double quotes like above will also treat most things literally but interprets ${variable}, $(sub-command), \$, \\, and \"
    # for echo in particular, need to add -e option because default w/o it is to disallow interpretting special characters
  done <<< "${@:-}"
}

# exit terminates the script returning specified exit value (1 in this example)
function emergency () {                                __b3bp_log emergency "${@}"; exit 1; }
function alert ()     { [[ "${LOG_LEVEL:-0}" -ge 1 ]] && __b3bp_log alert "${@}"; true; }
function critical ()  { [[ "${LOG_LEVEL:-0}" -ge 2 ]] && __b3bp_log critical "${@}"; true; }
function error ()     { [[ "${LOG_LEVEL:-0}" -ge 3 ]] && __b3bp_log error "${@}"; true; }
function warning ()   { [[ "${LOG_LEVEL:-0}" -ge 4 ]] && __b3bp_log warning "${@}"; true; }
function notice ()    { [[ "${LOG_LEVEL:-0}" -ge 5 ]] && __b3bp_log notice "${@}"; true; }
function info ()      { [[ "${LOG_LEVEL:-0}" -ge 6 ]] && __b3bp_log info "${@}"; true; }
function debug ()     { [[ "${LOG_LEVEL:-0}" -ge 7 ]] && __b3bp_log debug "${@}"; true; }

function help () {
  echo "" 1>&2
  echo " ${*}" 1>&2
  echo "" 1>&2
  echo "  ${__usage:-No usage available}" 1>&2
  echo "" 1>&2

  if [[ -n "${__helptext:-}" ]]; then
    echo " ${__helptext}" 1>&2
    echo "" 1>&2
  fi

  exit 1
}


### Parse commandline options from __usage and __helptext as defined in calling script
##############################################################################

# Commandline options. This defines the usage page, and is used to parse cli
# opts & defaults from. The parsing is unforgiving so be precise in your syntax
# - A short option must be preset for every long option; but every short option
#   need not have a long option (although long options are certainly preferred for clarity)
# - `--` is respected as the separator between options and arguments (see handling of this at end of this section)
# - We do not bash-expand defaults, so setting '~/app' as a default will not resolve to ${HOME}/app.
#   you can use bash variables to work around this (i.e. use ${HOME} instead of ~)

# There are two reserved characters which cannot be used as options when using getopts as we do below to parse - the colon (":") and the question mark ("?")
# examples of each best practice below e.g. w/ and w/o arg, required options, default values, etc
# follow examples exactly e.g. parser looks for "Required." with initial cap & period and similarly expects Default="value" with quotes
# "Can be repeated." is also looked for by parser exactly like this
# {arg} is the same as "Required." in description, seems latter is better from usability perspective
# EXAMPLE __usage:
#read -r -d '' __usage <<-'EOF'
  # -f --file  [arg] Filename to process. Required.
  # -t --temp  [arg] Location of tempfile. Default="/tmp/bar"
  # -v               Enable verbose mode, print script as it is executed
  # -d --debug       Enables debug mode
  # -h --help        These usage details
  # -n --no-color    Disable color output
  # -1 --one         Do just one thing
  # -i --input [arg] File to process. Can be repeated.
  # -o --output {arg} File to write output.
  # -x               Specify a flag. Can be repeated.
#EOF

# Additional help text to print after usage
# EXAMPLE __helptext:
#read -r -d '' __helptext <<-'EOF'
 # This script does.........
 # It assumes.....
 # It doesn't work for......
#EOF

# Confirm __usage and __helptext are defined
if [[ -z ${__usage:-} ]]; then
  help "The usage statement defining command line options must be in the variable named __usage"
elif [[ -z ${__helptext:-} ]]; then
  help "A blurb of help text explaining what the script is doing must be defined in the variable named __helptext"
fi

# Parse usage string in preparation for calling getopts below
# and set $arg_<flag> defaults (where flag is the single character form of option e.g. f instead of file)
while read -r __b3bp_tmp_line; do
  if [[ "${__b3bp_tmp_line}" =~ ^- ]]; then
    # fetch single character version of option string
    __b3bp_tmp_opt="${__b3bp_tmp_line%% *}"
    __b3bp_tmp_opt="${__b3bp_tmp_opt:1}"

    # fetch long version if present
    __b3bp_tmp_long_opt=""

    if [[ "${__b3bp_tmp_line}" == *"--"* ]]; then
      __b3bp_tmp_long_opt="${__b3bp_tmp_line#*--}"
      __b3bp_tmp_long_opt="${__b3bp_tmp_long_opt%% *}"
    fi

    # map opt long name to+from opt short name
    printf -v "__b3bp_tmp_opt_long2short_${__b3bp_tmp_long_opt//-/_}" '%s' "${__b3bp_tmp_opt}"
    printf -v "__b3bp_tmp_opt_short2long_${__b3bp_tmp_opt}" '%s' "${__b3bp_tmp_long_opt//-/_}"

    # check if option takes an argument
    if [[ "${__b3bp_tmp_line}" =~ \[.*\] ]]; then
      __b3bp_tmp_opt="${__b3bp_tmp_opt}:" # add : if opt has arg - this will be required by getopts below
      __b3bp_tmp_init=""  # it has an arg. init with ""
      printf -v "__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}" '%s' "1"
    elif [[ "${__b3bp_tmp_line}" =~ \{.*\} ]]; then
      __b3bp_tmp_opt="${__b3bp_tmp_opt}:" # add : if opt has arg
      __b3bp_tmp_init=""  # it has an arg. init with ""
      # remember that this option requires an argument (really, that the option itself is required since all arg-taking options require the arg if they are specified)
      # e.g. can't have --input  --file where --input has no arg even though it is defined in usage as --input [arg] since [] doesn't mean the arg itself is optional
      # [] means the whole -i option is optional although this can be over-ridden with "Required." in description as with --file where the "Required." is parsed below
      printf -v "__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}" '%s' "2"
    else
      __b3bp_tmp_init="0" # it's a flag. init with 0 for false
      printf -v "__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}" '%s' "0"
    fi
    # This is cumulative concatenation of all short options w/o any delimiter, required by getopts below to parse runtime values
    # Per getopts, those options taking an arg are actually o: so for the example usage statement above, this becomes f:t:vdhnli:o:x
    __b3bp_tmp_opts="${__b3bp_tmp_opts:-}${__b3bp_tmp_opt}"

    if [[ "${__b3bp_tmp_line}" =~ ^Can\ be\ repeated\. ]] || [[ "${__b3bp_tmp_line}" =~ \.\ *Can\ be\ repeated\. ]]; then
      # remember that this option can be repeated
      printf -v "__b3bp_tmp_is_array_${__b3bp_tmp_opt:0:1}" '%s' "1"
    else
      printf -v "__b3bp_tmp_is_array_${__b3bp_tmp_opt:0:1}" '%s' "0"
    fi
  fi

  # If line didn't start with expected single character option, continue to next line
  [[ "${__b3bp_tmp_opt:-}" ]] || continue

  if [[ "${__b3bp_tmp_line}" =~ ^Default= ]] || [[ "${__b3bp_tmp_line}" =~ \.\ *Default= ]]; then
    # ignore default value if option does not have an argument (defined suffixes above as :0=no arg, :1=arg_for_optional, :2=arg_for_required)
    __b3bp_tmp_varname="__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}"
    if [[ "${!__b3bp_tmp_varname}" != "0" ]]; then
      # take default
      __b3bp_tmp_init="${__b3bp_tmp_line##*Default=}"
      # strip double quotes from default argument
      __b3bp_tmp_re='^"(.*)"$'
      if [[ "${__b3bp_tmp_init}" =~ ${__b3bp_tmp_re} ]]; then
        # BASH_REMATCH stores regex results from above =~ test in if condition
        __b3bp_tmp_init="${BASH_REMATCH[1]}"
      else
        # strip single quotes from default argument
        __b3bp_tmp_re="^'(.*)'$"
        if [[ "${__b3bp_tmp_init}" =~ ${__b3bp_tmp_re} ]]; then
          __b3bp_tmp_init="${BASH_REMATCH[1]}"
        fi
      fi
    fi
  fi

  # Alternative to {arg} in usage to indicate the option is required (arg is always required if it is an arg-taking option and option is used)
  if [[ "${__b3bp_tmp_line}" =~ ^Required\. ]] || [[ "${__b3bp_tmp_line}" =~ \.\ *Required\. ]]; then
    # remember that this option requires an argument
    printf -v "__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}" '%s' "2"
  fi
  
  # Init var with value unless it is an array / a repeatable (defined suffixes above as :1=repeatable, :0=not)
  __b3bp_tmp_varname="__b3bp_tmp_is_array_${__b3bp_tmp_opt:0:1}"
  [[ "${!__b3bp_tmp_varname}" == "0" ]] && printf -v "arg_${__b3bp_tmp_opt:0:1}" '%s' "${__b3bp_tmp_init}"
done <<< "${__usage:-}"


# run getopts only if options were specified in __usage
# getopts parses command line args - see bash man page
if [[ -n "${__b3bp_tmp_opts:-}" ]]; then
  # Allow long options like --this
  # Since getopts will assume single char options, it will read --option as if the single char flag is "-" so adding "-" to list
  # Specifically, adding as -: so that everything after -- will be put into OPTARG
  __b3bp_tmp_opts="${__b3bp_tmp_opts}-:"

  # Reset in case getopts has been used previously in the shell e.g. by a parent script
  # So, getopts will start with whatever is currently in $@ at the beginning
  OPTIND=1

  # start parsing command line
  set +o nounset # unexpected arguments will cause unbound variables
                 # to be dereferenced so temporarily disabling nounset option (it is re-enabled below)
  # Overwrite any existing $arg_<flag> defaults with the actual CLI options
  # getopts assumes single-char options and handles -vdx where v, d, and x are each separate options
  # the __b3bp_tmp_opt iterator will take the value of each single character option as sequentially observed in $@
  # if an argument is expected (see definition of __b3bp_tmp_opts when parsing usage statement above), the word after -o is stored in OPTARG (if arg expected, is required, no such thing as an "optional" arg)
  # if no argument is expected, OPTARG is empty (or null/undefined?)
  # OPTIND will contain the index of the command line arg to process in the next iteration
  # e.g. "main.sh -f file -x -t tmp" the first iteration will have __b3bp_tmp_opt as "f", OPTARG as "file", and OPTIND as 3 pointing to -x in $3
  # second iteration will have __b3bp_tmp_opt as "x", OPTARG as "" (or null/undefined?), and OPTIND as 4 pointing to -t in $4
  # tested & confirmed OPTARG is actually "cleared" for -x which doesn't expect arg instead of just remaining as "file" from prior iteration
  # when have "main.sh -vdx" first iteration will have __b3bp_tmp_opt as "v" but OPTIND will remain as 1 since still the dx options to parse from $1
  # when parsing an expected arg, if space missing between option and arg, puts everything after -o until next space in OPTARG
  # e.g. "main.sh -file -x -t tmp" the first iteration will have __b3bp_tmp_opt as "f", OPTARG as "ile", and OPTIND as 2 pointing to -x in $2
  # BUG ? this means if accidentally put -long-name arg_value, will get unexpected results
  # e.g. "main -file /path/to/file -x" the first iteration will have __b3bp_tmp_opt as "f", OPTARG as "ile", and OPTIND as 2 pointing to /path/to/file in $2
  # and next iteration will see /path/to/file as a non-option and terminate loop WITHOUT ERROR thus failing to parse -x
  # similarly, if omit expected arg in -vdx style, unexpected results
  # e.g. "main -fxv -x" the first iteration will have __b3bp_tmp_opt as "f", OPTARG as "xv", and OPTIND as 2 pointing to -x in $2 so both arg_f is wrong but also the xv are not processed correctly
  # but, no way to know the xv in -fxv are options UNLESS require a space before arg and therefore w/o space can assume are options and die as missing arg
    while getopts "${__b3bp_tmp_opts}" __b3bp_tmp_opt; do
    # If getopts observes a -o not present in __b3bp_tmp_opts or fails to find an expected arg, sets the __b3bp_tmp_opt iterator to "?"
    # ${*} is the command line without the script like $@ but subtly different - need to investigate when to use which
    [[ "${__b3bp_tmp_opt}" == "?" ]] && help "Invalid use of script: ${*} "

    if [[ "${__b3bp_tmp_opt}" == "-" ]]; then
      # OPTARG is long-flag-name or long-option-with-arg=value or long-option-with-arg  (where value is in next position where OPTIND points)
      if [[ "${OPTARG}" =~ .*=.* ]]; then
        # --key=value format so OPTARG is "key=value"
        __b3bp_tmp_long_opt=${OPTARG/=*/}
        # Set opt to the short option corresponding to the long option
        __b3bp_tmp_varname="__b3bp_tmp_opt_long2short_${__b3bp_tmp_long_opt//-/_}"
        printf -v "__b3bp_tmp_opt" '%s' "${!__b3bp_tmp_varname}"
        OPTARG=${OPTARG#*=}
      else
        # --key value format or simply --key if no arg accepted
        # Map long name to short version of option
        __b3bp_tmp_varname="__b3bp_tmp_opt_long2short_${OPTARG//-/_}"
        printf -v "__b3bp_tmp_opt" '%s' "${!__b3bp_tmp_varname}"
        # Only assign OPTARG if option takes an argument
        __b3bp_tmp_varname="__b3bp_tmp_has_arg_${__b3bp_tmp_opt}"
        __b3bp_tmp_varvalue="${!__b3bp_tmp_varname}" #defined suffixes when parsing usage statement as :0=no arg, :1=arg_for_optional, :2=arg_for_required
        # after confirm an arg is expected, no longer need to distinguish b/w optional and required and re-purposing __b3bp_tmp_varvalue to be +1 incrementor for OPTIND below
        [[ "${__b3bp_tmp_varvalue}" != "0" ]] && __b3bp_tmp_varvalue="1"
        # UNCLEAR @: syntax but expected behavior is OPTARG will have value of next arg per OPTIND OR be cleared if no arg expected
        printf -v "OPTARG" '%s' "${@:OPTIND:${__b3bp_tmp_varvalue}}"
        # now that have parsed OPTARG from OPTIND, increment OPTIND unless there was no arg expected in which case OPTIND already points to next option
        ((OPTIND+=__b3bp_tmp_varvalue))
      fi
      # we have set __b3bp_tmp_opt/OPTARG to the short value and the argument as OPTARG if it exists
    fi

    __b3bp_tmp_value="${OPTARG}"

    __b3bp_tmp_varname="__b3bp_tmp_is_array_${__b3bp_tmp_opt:0:1}"
    __b3bp_tmp_varname2="__b3bp_tmp_has_arg_${__b3bp_tmp_opt:0:1}"
    if [[ "${!__b3bp_tmp_varname}" != "0" ]]; then
      # repeatables
      # this original test relies on OPTARG to distinguish a flag option from an arg one which is less reliable than using how option defined in usage
      #if [[ -z "${OPTARG}" ]]; then
      # shellcheck disable=SC2016
      if [[ "${!__b3bp_tmp_varname2}" == "0" ]]; then
        # repeatable flags, they increcemnt e.g. -x -x -x results in arg_x being 3
        __b3bp_tmp_varname="arg_${__b3bp_tmp_opt:0:1}"
        # BUG, using __b3bp_tmp_default which hasn't been set for this variable and could carry over from prior
        # get current value (possibly default set when parsing usage above) to report (curr) -> new in debug statement below
        __b3bp_tmp_default="${!__b3bp_tmp_varname}"
        # shellcheck disable=SC2004
        __b3bp_tmp_value=$((${!__b3bp_tmp_varname} + 1))
        printf -v "${__b3bp_tmp_varname}" '%s' "${__b3bp_tmp_value}"
        debug "cli arg ${__b3bp_tmp_varname} = (${__b3bp_tmp_default}) -> ${!__b3bp_tmp_varname}"
      else
        # repeatable args, they get appended to an array
        __b3bp_tmp_varname="arg_${__b3bp_tmp_opt:0:1}[@]"
        debug "cli arg ${__b3bp_tmp_varname} append ${__b3bp_tmp_value}"
        declare -a "${__b3bp_tmp_varname}"='("${!__b3bp_tmp_varname}" "${__b3bp_tmp_value}")'
      fi
    else
      # non-repeatables
      __b3bp_tmp_varname="arg_${__b3bp_tmp_opt:0:1}"
      # get current value (possibly default set when parsing usage above) to report (curr) -> new in debug statement below
      __b3bp_tmp_default="${!__b3bp_tmp_varname}"
      # If no arg, flipping flag from default 0 to 1 (or incrementing if default is 1)
      # this original test relies on OPTARG to distinguish a flag option from an arg one which is less reliable than using how option defined in usage
      #if [[ -z "${OPTARG}" ]]; then
      if [[ "${!__b3bp_tmp_varname2}" == "0" ]]; then
        __b3bp_tmp_value=$((__b3bp_tmp_default + 1))
      fi

      printf -v "${__b3bp_tmp_varname}" '%s' "${__b3bp_tmp_value}"

      debug "cli arg ${__b3bp_tmp_varname} = (${__b3bp_tmp_default}) -> ${!__b3bp_tmp_varname}"
    fi
  done
  set -o nounset # no more unbound variable references expected
  # popping off all the parsed options which generally is all of $@ but some situations where not
  # e.g. "--" alone is interpreted by getopts as end of options so everything after will remain in $@
  # also malformed syntax may cause premature stopping of getopts
  # e.g. "-file myfile -x -v" will see "ile" as arg to -f and then stop processing when myfile is observed in next iteration and considered to not be an option so "myfile -x -v" will remain in $@
  shift $((OPTIND-1))

  # getopts interprets "--" alone to be end of options list and stops processing leaving "-- any other -remaining command -line --args" in $@
  # this simply pops off the "--" since it was tecnically processed by getopts (i.e. as the terminating signal)
  if [[ "${1:-}" == "--" ]] ; then
    shift
  fi
fi


### Signal trapping and backtracing
##############################################################################

# requires `set -o errtrace`
__b3bp_err_report() {
    # $? stores exit value of last executed command
    local error_code=${?}
    # UNCLEAR if parameters can be interpreted when this isn't declared as a function - is it a BUG?
    error "Error in ${__file} in function ${1} on line ${2}"
    exit ${error_code}
}



### Automatic validation of required option arguments
##############################################################################
# shellcheck disable=SC2154
for __b3bp_tmp_varname in ${!__b3bp_tmp_has_arg_*}; do
  # validate only options which required an argument
  # since OR statement, if =2 which is how required were defined, won't execute the continue
  [[ "${!__b3bp_tmp_varname}" == "2" ]] || continue
  
  __b3bp_tmp_opt_short="${__b3bp_tmp_varname##*_}"
  __b3bp_tmp_varname="arg_${__b3bp_tmp_opt_short}"
  # since AND statement, if arg_f is not null and therefore true, will execute continue attempting to get all trues
  [[ "${!__b3bp_tmp_varname}" ]] && continue

  __b3bp_tmp_varname="__b3bp_tmp_opt_short2long_${__b3bp_tmp_opt_short}"
  printf -v "__b3bp_tmp_opt_long" '%s' "${!__b3bp_tmp_varname}"
  # if long-name was defined, translating back to original replacing _ with -
  [[ "${__b3bp_tmp_opt_long:-}" ]] && __b3bp_tmp_opt_long=" (--${__b3bp_tmp_opt_long//_/-})"

  help "Option -${__b3bp_tmp_opt_short}${__b3bp_tmp_opt_long:-} requires an argument"
done


### Runtime
##############################################################################

info "__file: ${__file}"
info "__dir: ${__dir}"
info "__base: ${__base}"
info "OSTYPE: ${OSTYPE}"
if [[ -n ${*:-} ]]; then
  notice "Remaining command line after parsing options may indicate incomplete parsing due to malformation: ${*}"
fi

# Loop through all initialized options reporting them at info level
# shellcheck disable=SC2154
for __b3bp_tmp_varname in ${!arg_*}; do
  # this wasn't working in cases like "--input= -x -i input3" where -i/--input takes arg
  #if [[ -n "${!__b3bp_tmp_varname}" ]] && declare -p ${__b3bp_tmp_varname} 2> /dev/null | grep -q '^declare \-a'; then
  # grep returns exit status of 0 for success finding match or non-zero if no match is found
  # if without [[ condition ]] interprets success exit=0 as true and error exit!=0 as false
  # need to add the && true because set -o errexit which will die when the grep fails to find a match
  if declare -p ${__b3bp_tmp_varname} 2> /dev/null | grep -q '^declare \-a' && true; then
    info "${__b3bp_tmp_varname}:"
    # shellcheck disable=SC1083
    eval __b3bp_tmp_array=\( \${"${__b3bp_tmp_varname}[@]"} \)
    for __b3bp_tmp_element in "${__b3bp_tmp_array[@]}"; do
      info " - ${__b3bp_tmp_element}"
    done
  elif [[ -n "${!__b3bp_tmp_varname}" ]]; then
    info "${__b3bp_tmp_varname}: ${!__b3bp_tmp_varname}"
  else
    # All that remain are flags which must default to 0
    # Since any missing arg-options would be optional since didn't error on validation above
    info "${__b3bp_tmp_varname}: 0"
  fi
done


### Cleanup Environment variables
# Also had to move this with the above validation block to below -h flag execution because it can't happen before the validation block which uses these variables
##############################################################################
# shellcheck disable=SC2154
for __tmp_varname in ${!__b3bp_tmp_*}; do
  unset -v "${__tmp_varname}"
done

unset -v __tmp_varname

