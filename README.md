# Initialize_Bash_script

Script to source at the start of a bash script to parse command line arguments etc

Derived from https://github.com/kvz/bash3boilerplate

http://bash3boilerplate.sh/#authors

Copyright (c) 2013 Kevin van Zonneveld and contributors